#!/bin/bash

for i in $@;do
    openscad -D "vol=$i" -o ./gauges/$i.stl gauge.scad
    echo Created $i gauge
done;
