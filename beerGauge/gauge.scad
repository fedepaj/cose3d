$fn = 60; 

PI = 3.14159265359;
//volumi in mm^3
/*gauge(1826.55);
translate([0,25,0]) gauge(2767.5);
translate([0,50,0]) gauge(2029.5);
translate([0,75,0]) gauge(3075);
translate([0,100,0]) gauge(2232.45);
translate([0,125,0]) gauge(3382.5);
translate([0,150,0]) */
vol=1000;
gauge(vol);
module gauge(vol){
    //GAUGE CYLINDER
    radius = (pow((vol/(1.7*PI)),1/3))+0.4;
    height = 1.7*radius;
    translate([0,0,1.2]) cylinder(r=radius,h=height);
    
    //GAUGE HANDLE
    difference() {
        hull() {
            cylinder(r=radius,h=1.2);
            translate([(height*2)+2,0,0]) cylinder(r=radius,h=1.2);
        }
        translate([(height*2)+2,0,-2]) cylinder(r=2,h=5); //HANDLE HOLE
        translate([0,radius/2,0])  mirror([0,1,0])linear_extrude(0.6) text(str(vol),size=(radius), font=":style=Bold"); //MEASURE
    }   
}

