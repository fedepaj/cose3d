headDiameter=15; //[10:1:30]
headHeight=6; //[5:1:20]
boltDiameter=8; //[5:1:20]
cl=1.2;
module hex(hexO,h){
    $fn=6;
    cylinder(d=hexO, h=h);
}

module knob(headO,headH,boltO){
    difference(){
        hex(headO+cl,headH);
        cylinder(d=boltO+cl,h=headH);
    }
    difference(){
        hex(headO*2.5,headH*3);
        hex(headO,headH*3);
    }
}

knob(headDiameter,headHeight,boltDiameter);